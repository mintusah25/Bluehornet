Setup Steps
===========


1. Environment Setup:

    * Create a virtual environment

    * Install the requirements by using command:
        * pip install -r `file_name`
            - If project is running on your local machine use `reqs/local.txt`
            - If project is running on your production machine use `requirements.txt`

2. Database Setup (Development environment):

    * Setup a database server (preferably Postgres) and:
        * Create a database with name 'bluehornet_dev'(if using postgres)
        * Or
        * If you want a custom setup, provide credentials in settings/dev_local.py file(Created using settings/dev_local.py.template).

    * App Migrations:
        * Run `python manage.py migrate`

3. Setup git pre-commit hook
   * Create a file under .git/hook/ with name pre-commit
        * /project/root/.git/hooks/pre-commit
   * Add following to that file
        #!/usr/bin/env bash
        /path/to/git-pylint-commit-hook --pylint /path/to/pylint --pylintrc pylint.rc
   * /path/to/ is path where git-pylint-commit-hook and pylint exists.


==================================
