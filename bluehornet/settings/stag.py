"""Staging settings and globals."""


from . import common


class Settings(common.Settings):
    ########## DEBUG CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
    DEBUG = False
    ########## END DEBUG CONFIGURATION

    ########## DATABASE CONFIGURATION

    ########## END DATABASE CONFIGURATION

    ########## EMAIL CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
    EMAIL_BACKEND = ''

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host
    EMAIL_HOST = ''

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host-password
    EMAIL_HOST_PASSWORD = ''

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host-user
    EMAIL_HOST_USER = ''

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#email-port
    EMAIL_PORT = '587'

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#email-subject-prefix
#     EMAIL_SUBJECT_PREFIX = '[{0}] '.format(common.Settings.SITE_NAME)

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#email-use-tls
    EMAIL_USE_TLS = True

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#server-email
    SERVER_EMAIL = property(lambda self: self.EMAIL_HOST_USER)
    ########## END EMAIL CONFIGURATION

    ########## APPS CONFIGURATION
    # See: http://django-storages.readthedocs.org/en/latest/index.html
    THIRD_PARTY_APPS = common.Settings.THIRD_PARTY_APPS + (
        'storages',
        'raven.contrib.django.raven_compat',  # Sentry
        'collectfast',
        #'clear_cache',
    )
    ########## END APPS CONFIGURATION

    ########## CACHE CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#caches

    ########## END CACHE CONFIGURATION

    ########## CELERY CONFIGURATION

    ########## END CELERY CONFIGURATION

    ########## COMPRESSION CONFIGURATION
    # See: http://django_compressor.readthedocs.org/en/latest/settings/#django.conf.settings.COMPRESS_ENABLED
    COMPRESS_ENABLED = True

    # See: http://django_compressor.readthedocs.org/en/latest/settings/#django.conf.settings.COMPRESS_OFFLINE
    COMPRESS_OFFLINE = False
    ########## END COMPRESSION CONFIGURATION

    ########## ALLOWED HOSTS CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
    ALLOWED_HOSTS = ['*']
    ########## END ALLOWED HOST CONFIGURATION

    ########## SENTRY
    RAVEN_CONFIG = {
        'dsn': '',
    }
    ########## END SENTRY
