"""Development settings and globals."""
from . import common


class Settings(common.Settings):
    DEVELOPMENT_ENV = True

    ########## DEBUG CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
    DEBUG = True

    ########## DATABASE CONFIGURATION

    ########## END DATABASE CONFIGURATION

    ########### EMAIL CONFIGURATION
    # # See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
    # EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
    ########### END EMAIL CONFIGURATION

    ########## CACHE CONFIGURATION
    ########## END CACHE CONFIGURATION

    ########### CELERY CONFIGURATION
    # # See: http://docs.celeryq.org/en/latest/configuration.html#celery-always-eager
    # CELERY_ALWAYS_EAGER = True
    #
    # # See: http://docs.celeryproject.org/en/latest/configuration.html#celery-eager-propagates-exceptions
    # CELERY_EAGER_PROPAGATES_EXCEPTIONS = True
    ########### END CELERY CONFIGURATION

    ########## TOOLBAR CONFIGURATION
    # See: https://github.com/django-debug-toolbar/django-debug-toolbar#installation

    ########## END TOOLBAR CONFIGURATION

    ########## DATABASE CONFIGURATION
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#databases

    ########## END DATABASE CONFIGURATION
